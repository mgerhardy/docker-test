FROM debian:experimental

LABEL maintainer "martin.gerhardy@gmail.com"

ENV DEBIAN_FRONTEND noninteractive

ENV ORACLE_INSTANTCLIENT_MAJOR 12.1
ENV ORACLE_INSTANTCLIENT_VERSION 12.1.0.2.0
ENV ORACLE /usr/local/oracle
ENV ORACLE_HOME $ORACLE/lib/oracle/$ORACLE_INSTANTCLIENT_MAJOR/client64
ENV ORAINCS $ORACLE/include/oracle/$ORACLE_INSTANTCLIENT_MAJOR/client64
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:$ORACLE/lib/oracle/$ORACLE_INSTANTCLIENT_MAJOR/client64/lib
ENV C_INCLUDE_PATH $C_INCLUDE_PATH:$ORAINCS
ENV SONAR_RUNNER_HOME=/opt/sonar-scanner
ENV PATH $PATH:/opt/sonar-scanner/bin

RUN (cat /etc/apt/sources.list) && \
    (echo "deb http://http.debian.net/debian/ unstable main contrib non-free" > /etc/apt/sources.list) && \ 
    echo "force-unsafe-io" > /etc/dpkg/dpkg.cfg.d/02apt-speedup 

RUN apt-get update \
	&& apt-get install -y libaio1 unzip curl fakeroot alien \
	&& curl -L https://github.com/sergeymakinen/docker-oracle-instant-client/raw/assets/oracle-instantclient$ORACLE_INSTANTCLIENT_MAJOR-basic-$ORACLE_INSTANTCLIENT_VERSION-1.x86_64.rpm -o basic.rpm \
	&& curl -L https://github.com/sergeymakinen/docker-oracle-instant-client/raw/assets/oracle-instantclient$ORACLE_INSTANTCLIENT_MAJOR-devel-$ORACLE_INSTANTCLIENT_VERSION-1.x86_64.rpm -o devel.rpm \
	&& fakeroot alien *.rpm \
	&& ls *.rpm *.deb \
	&& dpkg -i *.deb \
	&& dpkg -l | grep oracle \
	&& rm *.rpm \
	&& rm *.deb \
	&& rm -rf /var/lib/apt/lists/* \
	&& apt-get purge -y --auto-remove rpm2cpio cpio \
	&& apt-get autoremove -y \
	&& apt-get clean -y
RUN apt-get update \
	&& apt-get install -y \
		pkg-config \
		build-essential \
		doxygen \
		cmake \
		cppcheck \
		gcc-7 \
		clang-4.0 \
		clang-format-4.0 \
    	clang-5.0 \
		ccache \
		valgrind \
		linux-perf \
		libxerces-c-dev \
		libcurl4-openssl-dev \
		libgsoap-dev \
		binutils-dev \
		libdw-dev \
		libcsv-dev \
		libssl1.0-dev \
		libyajl-dev \
		apache2-dev \
		libapr1-dev \
		libcppunit-dev \
		zlib1g-dev \
		libaprutil1-dev \
		xsdcxx \
		libreadonly-perl \
		libyaml-perl \
		libdigest-md5-file-perl \
		libpath-tiny-perl \
		liblog-log4perl-perl \
		libgetopt-complete-perl \
		libjson-validator-perl \
		libencode-perl \
		libmime-base64-urlsafe-perl \
		libtemplate-perl \
		libclass-date-perl \
		libconfig-general-perl \
		libmime-types-perl \
		libarray-compare-perl \
		libxml-sax-expat-perl \
		libnet-ssh-perl \
		libnet-scp-perl \
		libmath-fibonacci-perl \
		libconfig-general-perl \
		liblog-dispatch-filerotate-perl \
		libevent-perl \
		libhttp-body-perl \
		libnumber-format-perl \
		librest-client-perl \
		libio-tee-perl \
		libio-all-perl \
		libpoe-perl \
		libmro-compat-perl \
		libregexp-common-perl \
		libexpect-simple-perl \
		libtie-dxhash-perl \
		libipc-signal-perl \
		libjson-pp-perl \
		libhttp-server-simple-perl \
		libfile-tail-perl \
		libacme-damn-perl \
		libhttp-server-simple-psgi-perl \
		libipc-sharelite-perl \
		libmath-fibonacci-perl \
		libemail-mime-perl \
		libmail-imapclient-perl \
		libarray-utils-perl \
		libclass-date-perl \
		libsys-statistics-linux-perl \
		liblist-allutils-perl \
		libmail-sendmail-perl \
		libdata-compare-perl \
		libemail-messageid-perl \
		libspreadsheet-parseexcel-perl \
		libspreadsheet-writeexcel-perl \
		libspreadsheet-xlsx-perl \
		libproc-processtable-perl \
		libmail-sender-perl \
		libjcode-perl \
		libspiffy-perl \
		libcommon-sense-perl \
		liblist-rotation-cycle-perl \
		libmime-lite-perl \
		libole-storage-lite-perl \
		libbsd-resource-perl \
		libdancer-perl \
		libcurses-perl \
		libemail-sender-perl \
		liblog-dispatch-perl \
		libwww-mechanize-perl \
		liblist-compare-perl \
		libterm-readline-gnu-perl \
		libtext-template-perl \
		libfile-pid-perl \
		libexpect-perl \
		libclass-accessor-perl \
		libemail-simple-perl \
		libemail-mime-encodings-perl \
		libdevel-globaldestruction-perl \
		libsub-name-perl \
		libdbd-oracle-perl \
		libstatistics-descriptive-perl \
		libproc-daemon-perl \
		libhash-merge-perl \
		libclass-trigger-perl \
		libemail-mime-attachment-stripper-perl \
		libproc-pid-file-perl \
		libemail-mime-contenttype-perl \
		libinline-c-perl \
		libemail-date-format-perl \
		libfile-chdir-perl \
		libyaml-syck-perl \
		openjdk-7-jdk \
		openjdk-8-jdk \
		openjdk-9-jdk \
		python3-pip \
		curl \
		ant \
		maven \
		gradle \
	&& rm -rf /var/lib/apt/lists/* \
	&& apt-get autoremove -y \
	&& apt-get clean -y
RUN curl https://netix.dl.sourceforge.net/project/mod-gsoap/mod-gsoap/mod-gsoap-0.7/mod_gsoap-0.7.tar.gz | tar xvz mod_gsoap-0.7/apache_20/apache_gsoap.h -O > /usr/include/apache_gsoap.h
RUN pip3 install scan-build
RUN pip3 install gcovr
